/*
 * Usage: Varify the object/Variable is emapty or not .
 * Arguments: Variable / object to verify if it's empty or not.
 * Return: True or False.
 */

let isEmpty = (val) => {
    let typeOfVal = typeof val;
    switch (typeOfVal) {
        case 'object':
            return (val.length == 0) || !Object.keys(val).length;
        case 'string': {
            let str = val.trim();
            return str == '' || str == undefined || str === '{}' || str === 'null';
        }
        case 'number':
            return val == '';
        default:
            return val == '' || val == undefined;
    }
};
